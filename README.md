# walking-rabbit

Must have mysql in the machine, use a database named walking-rabbit


## Getting started

```
cd existing_repo
git clone https://gitlab.com/MixPollawat/walking-rabbit.git
```

## install Back-End

```
cd walking-rabbit/walking-rabbit/api
npm install
npm run dev
```

## install Front-End

```
cd walking-rabbit/walking-rabbit/frontend
npm install
npm start
```

## Import File Postman

```
open Postman
Import File Walking-Rabbit.postman_collection.json 
```

