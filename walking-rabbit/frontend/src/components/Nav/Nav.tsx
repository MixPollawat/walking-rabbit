import React from 'react';
import { Col, Nav, Row } from 'react-bootstrap';

function NavBar(props:any) {
  const { categories, setCategories } = props;

  return (
    <>
      <Nav className="mt-2" onSelect={(selectedKey) => setCategories(selectedKey)}>
        <Row>
            <Col key={0}>
              <Nav.Item className="border border-success">
                <Nav.Link className="text-center" eventKey={0}>All</Nav.Link>
              </Nav.Item>
            </Col>
            {
              categories.map((data:any)=>{
                return(
                  <Col key={data.id}>
                    <Nav.Item className="border border-success">
                      <Nav.Link className="text-center" eventKey={data.id}>{data.categoryName}</Nav.Link>
                    </Nav.Item>
                  </Col>
                );
              })
            }
        </Row>
      </Nav>
    </>
  );
}

export default NavBar;
