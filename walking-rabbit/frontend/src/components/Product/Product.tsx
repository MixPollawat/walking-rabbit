import React, { useCallback, useEffect, useState } from 'react';
import useIsMountedRef from 'src/hooks/useIsMountedRef';
import axiosInstance from '../.././utils/axios';
import { Container, Row, Col, Nav, Button, Modal, Form } from 'react-bootstrap';
import { useForm } from "react-hook-form";
import test from './../../images/3.png';

function Product(props:any) {

  const { register, handleSubmit, reset } = useForm();

  const { id } = props;

  const isMountedRef = useIsMountedRef();
  const [beverage, setBeverage] = useState<any[]>([]);
  const [dataBill, setDataBill] = useState<any>({});
  const [bill, setBill] = useState<any>({});
  const [dataModal, setDataModal] = useState<number>(0);
  const [money, setMoney] = useState<number>(0); // machine
  const [pay, setPay] = useState<number>(0); // beverage
  const [moneyPay, setMoneyPay] = useState<any>('');
  const [sumPay, setSumPay] = useState<number>(0);
  const [duration, setDuration] = useState<number>(0);
  const [counter, setCounter] = useState(-1);
  const [re, setRe] = useState(false);

  // Modal
  const [show, setShow] = useState(false);
  const [showPay, setShowPay] = useState(false);
  const [moneyChange, setMoneyChange] = useState(false);
  const [orderTime, setOrderTime] = useState(false);

  const handleMoneyChange = () => {
    setMoneyChange(true)
  };

  const handleCloseMoneyChange = () => {
    setMoneyChange(false)
  };

  const handleShowPay = () => {
    setShowPay(true)
  };

  const handlePay = () => {
    setSumPay(sumPay + moneyPay);
    if(sumPay + moneyPay >= pay){
      handleClosePay()
      handleMoneyChange()
    }
  };

  const handleClosePay = () => {
    setShowPay(false)
  };

  const handleShowOrderTime = () => {
    handleCloseMoneyChange();
    setCounter(duration);
    setOrderTime(true)
  };

  const handleCloseTime = () => {
    updateMoney();
    setOrderTime(false)
  };

  const updateMoney = useCallback(async () => {
    try {

      const updateMoney = money - (sumPay - pay);
      const responseMoney = await axiosInstance.patch("/api/machine",{money:updateMoney});
      await axiosInstance.post("/api/beverage/calBaverage",{id:dataBill.id});
      await axiosInstance.post("/api/bill",bill);

      if (isMountedRef.current) {
        setMoney(responseMoney.data.data.money);
        setBill({});
        setDuration(0);
        setMoneyPay(0);
        setSumPay(0);
        setRe(!re);
      }
    } catch (error) {
      console.error(error);
    }
  }, [isMountedRef, money, sumPay, pay, dataBill, bill, re]);

  const handleClose = () => {
    setShow(false)
  };

  const onInputPay = (event:any) => {
    setMoneyPay(event.target.value)
    if(event.target.value !== ''){
      setMoneyPay( parseInt( event.target.value ) )
    }
  };

  const handleShow = useCallback((event:any) => {
    reset()
    setDataModal(parseInt( event.target.value ))
    setDataBill( beverage.find((data)=> data.id === parseInt( event.target.value )))
    setShow(true)
  }, [dataBill,beverage]);

  const getBeverages = useCallback(async () => {
    try {
      let url = "/api/beverage"
      if(id > 0){
        url += `/getByCategory/${id}`
      }
      const responseBeverage = await axiosInstance.get(url);
      const responseMoney = await axiosInstance.get("/api/machine");

      if (isMountedRef.current) {
        setMoney(responseMoney.data.data.money)
        setBeverage(responseBeverage.data.data);
      }
    } catch (error) {
      console.error(error);
    }
  }, [isMountedRef,id]);

  useEffect(() => {
    if(counter <= 0){
      getBeverages();
    }
    if(counter > 0){
      setTimeout(()=> setCounter(counter - 1),1000);
    }
    console.log(sumPay);
    
  }, [id,counter,re]);

  return (
      <Container>
        <Row key={id}>
          {
            beverage.map((data:any)=>{
              return(
                <Col key={data.id} className="text-center mt-3 mb-3" xs={3}>
                  <div className="border border-secondary">
                    <span className='m-2'>Name : {data.beverageName}</span><br></br>
                    {data.categoryId === 3 ?
                          <span className='m-2'>Price : {data.price}</span> :
                          <>
                            <span className='m-2'>Price Hot : {data.price}</span><br></br>
                            <span className='m-2'>Price Cold : {data.priceCold}</span>
                          </>
                      }
                    <br></br>
                    {data.quantity > 0 ?
                    <Button className='m-2' variant="outline-success" value={data.id} onClick={handleShow}>BUY</Button>:
                    <Button className='m-2' variant="outline-danger" value={data.id} disabled>Sold Out</Button>
                    }
                  </div>
                </Col>
              );
            })
          }
        </Row>

        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Order</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className='text-center'>
              <span className='mb-1'>Name : {dataBill.beverageName}</span><br></br>
              {dataBill.categoryId === 3 ?
              <span className='mb-1'>Price : {dataBill.price}</span> :
              <>
                <span className='mb-1'>Price Hot : {dataBill.price}</span><br></br>
                <span className='mb-1'>Price Cold : {dataBill.priceCold}</span>
              </>
              }
              <br></br>
              <label className='m-1'>
                Buy :
                <select {...register("price")} className='ml-3'>
                  {dataBill.categoryId === 3 ?
                  <option value={`${dataBill.price}/-`}>Normal</option>
                  :
                  <>
                    <option value={`${dataBill.price}/Hot`}>Hot</option>
                    <option value={`${dataBill.priceCold}/Cold`}>Cold</option>
                  </>
                  }
                </select>
              </label>
              <br></br>
              <label className='m-1'>
                Sweetness :
                <select {...register("sweetness")} className='ml-3'>
                  {dataBill.categoryId === 3 ?
                  <option value='-'>-</option> :
                  <>
                    <option value='default'>Default</option>
                    <option value='less sugar'>Less Sugar</option>
                    <option value='more suger'>More Suger</option>
                  </>
                  }
                </select>
              </label>
              <br></br>
              <div className='mt-2'>
                <Form.Check
                  {...register("straw")}
                  inline
                  label=": Straw"
                  type={'checkbox'}
                />
                <Form.Check
                  {...register("cupCover")}
                  inline
                  label=": Cup Cover"
                  type={'checkbox'}
                />
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="outline-secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="outline-success" onClick={handleSubmit((data:any) => {
              data.beverageId = dataModal;
              data.beverageType = data.price.split("/")[1];
              data.price = data.price.split("/")[0];
              let duration = dataBill.durationHot;
              setShow(false)
              if(money - data.price < 0){
                alert("Not enough change")
                return false;
              }
              if(data.beverageType === 'Cold'){
                duration = dataBill.durationCold;
              }
              setBill(data)
              setPay(data.price)
              setDuration(duration)
              handleShowPay();
            })}>
              BUY
            </Button>
          </Modal.Footer>
        </Modal>

        <Modal show={showPay}>
          <Modal.Header closeButton>
            <Modal.Title>กรุณาชำระเงิน....</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className='text-center'>
              ยอดชำระ : {pay} B
              <br></br>
              คงเหลือยอดที่ต้องชำระ : { Math.abs( sumPay - pay ) } B
              <label>
                จำนวนเงินที่ชำระ : <input id='moneyPay' type='number' name='moneyPay'
                                value={moneyPay}
                                onChange={onInputPay}/>
              </label>
              <Button variant="outline-secondary" onClick={handlePay}>
                Pay
              </Button>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="outline-secondary" onClick={handleClosePay}>
              Back
            </Button>
          </Modal.Footer>
        </Modal>

        <Modal show={orderTime}>
          <Modal.Header closeButton>
            <Modal.Title>กำลังเตรียมเครื่องดื่ม....</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {counter > 0 ? <>จัดเตรียมภายใน : {counter} s </>:<> จัดเตรียมเสร็จสิ้น กรุณารับเครื่องดื่ม </>}
          </Modal.Body>
          <Modal.Footer>
            {counter === 0 ?
            <Button variant="outline-secondary" onClick={handleCloseTime}>
              OK
            </Button>
            : null
            }
          </Modal.Footer>
        </Modal>

        <Modal show={moneyChange}>
          <Modal.Header closeButton>
            <Modal.Title>ชำระเงินเรียบร้อย....</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            เงินทอน : {sumPay - pay} B
          </Modal.Body>
          <Modal.Footer>
            <Button variant="outline-secondary" onClick={handleShowOrderTime}>
              OK
            </Button>
          </Modal.Footer>
        </Modal>

      </Container>
  );
}

export default Product;
