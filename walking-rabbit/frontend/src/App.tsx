import React, { useCallback, useEffect, useState } from 'react';
import useIsMountedRef from 'src/hooks/useIsMountedRef';
import axiosInstance from './utils/axios';
import { Container, Row, Col } from 'react-bootstrap';
import Navbar from '../src/components/Nav';
import Product from './components/Product';
// import logo from './logo.svg';

function App() {
  const isMountedRef = useIsMountedRef();
  const [categoriestab, setCategoriestab] = useState<number>(0);
  const [categories, setCategories] = useState<any[]>([]);

  const getCategories = useCallback(async () => {
    try {
      const response = await axiosInstance.get('/api/categories');

      if (isMountedRef.current) {
        setCategories(response.data.data);
      }
    } catch (error) {
      console.error(error);
    }
  }, [isMountedRef]);

  useEffect(() => {
    getCategories();
  }, [getCategories,categoriestab]);

  return (
      <Container>
        <div className='text-center mt-3 mb-5'>
          <p className="fs-1">Walking - Rabbit</p>
        </div>
        <Navbar categories={categories} setCategories={setCategoriestab}/>
        <Product key={`${categoriestab}`} id={categoriestab} />
      </Container>
  );
}

export default App;
