module.exports = {
  port: process.env.PORT || 3001,
  host: process.env.DB_HOST ?? 'localhost',
  password: process.env.DB_PASSWORD ?? null,
  database: process.env.DB_DATABASE ?? 'walking_rabbit',
  username: process.env.DB_USERNAME ?? 'root',
  dbport: process.env.DB_PORT ?? 3306,
  dialect: process.env.DB_DIALECT ?? 'mysql',
};
