module.exports = (sequelize, DataTypes) => {
  const Machine = sequelize.define('machines', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    money: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0,
    },
  });

  return Machine;
};
