module.exports = (sequelize, DataTypes) => {
  const Bill = sequelize.define('bills', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    beverageId: {
      type: DataTypes.INTEGER,
      allowNull:false,
    },
    beverageType: {
      type: DataTypes.ENUM('Hot','Cold','-'),
      defaultValue: '-',
    },
    sweetness: {
      type: DataTypes.ENUM('less sugar','default','more suger','-'),
      defaultValue: '-',
    },
    straw: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    cupCover: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    price: {
      type: DataTypes.INTEGER,
      allowNull:false,
    },
  });

  return Bill;
};
