module.exports = (sequelize, DataTypes) => {
  const Beverage = sequelize.define('beverages', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    categoryId: {
      type: DataTypes.INTEGER,
      allowNull:false,
    },
    beverageName: {
      type: DataTypes.STRING(50),
      allowNull:false,
      unique: true
    },
    price: {
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false,
    },
    priceCold: {
      type: DataTypes.DECIMAL(10, 2),
      defaultValue: 0,
    },
    durationHot: {
      type: DataTypes.INTEGER(3),
      allowNull: false,
    },
    durationCold: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    quantity: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    }
  });

  return Beverage;
};
