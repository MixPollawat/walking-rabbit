module.exports = (sequelize, DataTypes) => {
  const Category = sequelize.define('categories', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    categoryName: {
      type: DataTypes.STRING(50),
      allowNull:false,
      unique: true
    },
  });

  return Category;
};
