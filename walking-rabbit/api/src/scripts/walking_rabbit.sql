-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 04, 2022 at 04:06 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+07:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `walking_rabbit`
--

-- --------------------------------------------------------

--
-- Table structure for table `beverages`
--

CREATE TABLE `beverages` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `beverage_name` varchar(50) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `price_cold` decimal(10,2) DEFAULT 0.00,
  `duration_hot` int(3) NOT NULL,
  `duration_cold` int(11) DEFAULT 0,
  `quantity` int(11) DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `beverages`
--

INSERT INTO `beverages` (`id`, `category_id`, `beverage_name`, `price`, `price_cold`, `duration_hot`, `duration_cold`, `quantity`, `created_at`, `updated_at`) VALUES
(1, 1, 'Espresso', '25.00', '30.00', 35, 40, 10, '2022-09-27 18:48:37', '2022-09-27 18:48:37'),
(2, 1, 'Americano', '30.00', '35.00', 40, 45, 10, '2022-09-27 18:50:04', '2022-10-04 11:27:33'),
(3, 1, 'Latte', '35.00', '40.00', 45, 50, 10, '2022-09-27 18:50:45', '2022-10-04 11:45:19'),
(4, 2, 'Taiwan Tea', '35.00', '40.00', 35, 40, 10, '2022-09-27 18:51:25', '2022-09-27 18:51:25'),
(5, 2, 'Thai Tea', '25.00', '30.00', 35, 40, 10, '2022-09-27 18:51:53', '2022-10-04 20:30:43'),
(6, 3, 'Soda', '15.00', '0.00', 0, 0, 10, '2022-09-27 18:52:17', '2022-10-04 11:14:50'),
(7, 3, 'Cola', '15.00', '0.00', 0, 0, 10, '2022-09-27 18:52:56', '2022-09-27 18:52:56'),
(8, 3, 'Energy Drink', '20.00', '0.00', 0, 0, 10, '2022-09-27 18:53:05', '2022-10-04 11:43:29');

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

CREATE TABLE `bills` (
  `id` int(11) NOT NULL,
  `beverage_id` int(11) NOT NULL,
  `beverage_type` enum('Hot','Cold','-') DEFAULT '-',
  `sweetness` enum('less sugar','default','more suger','-') DEFAULT '-',
  `straw` tinyint(1) DEFAULT 0,
  `cup_cover` tinyint(1) DEFAULT 0,
  `price` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `created_at`, `updated_at`) VALUES
(1, 'Coffee', '2022-09-27 18:44:26', '2022-09-27 18:44:26'),
(2, 'Tea', '2022-09-27 18:44:51', '2022-09-27 18:44:51'),
(3, 'Soft Drink', '2022-09-27 18:44:51', '2022-09-27 18:44:51');

-- --------------------------------------------------------

--
-- Table structure for table `machines`
--

CREATE TABLE `machines` (
  `id` int(11) NOT NULL,
  `money` decimal(10,2) DEFAULT 0.00,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `machines`
--

INSERT INTO `machines` (`id`, `money`, `created_at`, `updated_at`) VALUES
(1, '100000.00', '2022-10-04 15:17:41', '2022-10-04 20:30:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `beverages`
--
ALTER TABLE `beverages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `beverage_name` (`beverage_name`),
  ADD KEY `FK_Category` (`category_id`);

--
-- Indexes for table `bills`
--
ALTER TABLE `bills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Beverage` (`beverage_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `category_name` (`category_name`);

--
-- Indexes for table `machines`
--
ALTER TABLE `machines`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `beverages`
--
ALTER TABLE `beverages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `bills`
--
ALTER TABLE `bills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `machines`
--
ALTER TABLE `machines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `beverages`
--
ALTER TABLE `beverages`
  ADD CONSTRAINT `FK_Category` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `bills`
--
ALTER TABLE `bills`
  ADD CONSTRAINT `FK_Beverage` FOREIGN KEY (`beverage_id`) REFERENCES `beverages` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
