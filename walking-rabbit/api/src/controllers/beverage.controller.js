const { categories, beverages, bills, machines } = require('../models');
const sequelize = require("sequelize");

// Category
const createCategory = async (req, res) => {
  try {
    const [Categorie, created] = await categories.findOrCreate({
      where: { categoryName: req.body.categoryName },
      defaults: { categoryName: req.body.categoryName },
    });

    if (!created) {
      return res.status(400).json({ message: 'Category already exist' });
    }

    res.status(200).json({
      message: 'Category created successfully',
      data: Categorie,
    });
  } catch (errors) {
    res.status(500).send(errors.message);
  }
};

const getCattegories = async (req, res) => {
  try {
    const Categories = await categories.findAll();

    res.status(200).json({
      message: 'Get all baverages successfully',
      data: Categories,
    });
  } catch (error) {
    res.status(500).send(error.message);
  }
};

const getCategory = async (req, res) => {
  try {
    const Categorie = await categories.findOne({where:{
      id: req.params.id
    }});

    res.status(200).json({
      message: 'Get all baverages successfully',
      data: Categorie,
    });
  } catch (errors) {
    res.status(500).send(errors.message);
  }
};

const updateCategory = async (req, res) => {
  try {

    const Categorie = await categories.findOne({where:{
      id: req.params.id
    }});

    Categorie?.update(req.body);
    Categorie?.save();

    res.status(200).json({
      message: 'Get all baverages successfully',
      data: Categorie,
    });

  } catch (errors) {
    res.status(500).send(errors.message);
  }
};

const deleteCategory = async (req, res) => {
  try {

    await categories.destroy({
      where: {
        id: req.params.id
      },
    });

    res.status(200).json({
      message: 'Delete successfully',
    });

  } catch (errors) {
    res.status(500).send(errors.message);
  }
};

// Baverage
const createBaverage = async (req, res) => {
  try {

    const [Beverage, created] = await beverages.findOrCreate({
      where: { beverageName: req.body.beverageName },
      defaults: req.body ,
    });

    if (!created) {
      return res.status(400).json({ message: 'Baverage already exist' });
    }

    res.status(200).json({
      message: 'Baverage created successfully',
      data: Beverage,
    });
  } catch (error) {
    res.status(500).send(error.message);
  }
};

const getBaverages = async (req, res) => {
  try {
    const Beverages = await beverages.findAll({
      order: [
        ['quantity', 'DESC',],
      ]
    });

    res.status(200).json({
      message: 'Get all baverages successfully',
      data: Beverages,
    });
  } catch (error) {
    res.status(500).send(error.message);
  }
};

const getBaverage = async (req, res) => {
  try {

    const Baverage = await beverages.findOne({
      where:{
        id: req.params.id
      }
    });

    res.status(200).json({
      message: 'Get baverages successfully',
      data: Baverage,
    });
  } catch (error) {
    res.status(500).send(error.message);
  }
};

const getBaverageByCategoryId = async (req, res) => {
  try {

    const Baverages = await beverages.findAll({
      where:{
        categoryId: req.params.id
      },
      order: [
        ['quantity', 'DESC']
      ]
    });

    res.status(200).json({
      message: 'Get all baverages successfully',
      data: Baverages,
    });
  } catch (error) {
    res.status(500).send(error.message);
  }
};

const updateBaverage = async (req, res) => {
  try {
    const Baverage = await beverages.findOne({where:{
      id: req.params.id
    }});

    Baverage?.update(req.body);
    Baverage?.save();

    res.status(200).json({
      message: 'Baverage updated successfully',
      data: Baverage,
    });
  } catch (error) {
    res.status(500).send(error.message);
  }
};

const deleteBaverage = async (req, res) => {
  try {
    await beverages.destroy({
      where: {
        id: req.params.id
      },
    });

    res.status(200).json({
      message: 'Delete successfully',
    });
  } catch (error) {
    res.status(500).send(error.message);
  }
};

const calBaverage = async (req, res) => {
  try {
    const Beverage = await beverages.findOne(
      { where: { id: req.body.id } }
    );

    Beverage?.update({ quantity: Beverage.quantity - 1 });
    Beverage?.save();

    res.status(200).json({
      message: 'update successfully',
      data: Beverage,
    });
  } catch (error) {
    res.status(500).send(error.message);
  }
};

// Bill
const createBill = async (req, res) => {
  try {
    const newBill = {
      beverageId: req.body.beverageId ,
      beverageType: req.body.beverageType ,
      sweetness: req.body.sweetness ,
      straw: req.body.straw ,
      cupCover: req.body.cupCover ,
      price: req.body.price ,
    };

    const Bill = await bills.create(newBill)

    res.status(200).json({
      message: 'Bill created successfully',
      data: Bill,
    });
  } catch (error) {
    res.status(500).send(error.message);
  }
};

const getBills = async (req, res) => {
  try {
    const Bills = await bills.findAll();

    res.status(200).json({
      message: 'Get all Bills successfully',
      data: Bills,
    });
  } catch (error) {
    res.status(500).send(error.message);
  }
};

const getAllMoney = async (req, res) => {
  try {
    const Bills = await bills.findAll({
      attributes: [
      [sequelize.fn('COUNT', sequelize.col('price')), 'sumBill'],
      [sequelize.fn('sum', sequelize.col('price')), 'sumMoney'],
      ]
    });

    res.status(200).json({
      message: 'Get Money successfully',
      data: Bills,
    });
  } catch (error) {
    res.status(500).send(error.message);
  }
};

// Machines
const getMachines = async (req, res) => {
  try {
    const Machines = await machines.findOne(
      { where: { id: 1 } }
    );

    res.status(200).json({
      message: 'Get all Bills successfully',
      data: Machines,
    });
  } catch (error) {
    res.status(500).send(error.message);
  }
};

const updateMachines = async (req, res) => {
  try {
    const Machines = await machines.findOne(
      { where: { id: 1 } }
    );

    Machines?.update({ money: req.body.money });
    Machines?.save();

    res.status(200).json({
      message: 'update successfully',
      data: Machines,
    });
  } catch (error) {
    res.status(500).send(error.message);
  }
};

module.exports = {
  // category
  createCategory,
  getCattegories,
  getCategory,
  updateCategory,
  deleteCategory,

  // baverage
  createBaverage,
  getBaverages,
  getBaverage,
  getBaverageByCategoryId,
  updateBaverage,
  deleteBaverage,
  calBaverage,

  //bill
  createBill,
  getBills,
  getAllMoney,

  // machines
  getMachines,
  updateMachines,
};
