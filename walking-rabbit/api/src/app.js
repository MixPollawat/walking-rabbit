require('dotenv').config();

// ----------------------------------------------------------------
// Required External Modules

const express = require('express');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');
const { sequelize, categories, beverages, bills } = require('./models');
const config = require('./config');
const router = require('./routes');
const Runner = require("run-my-sql-file");
// ----------------------------------------------------------------
// App Configuration

// defining the Express app
const app = express();

// adding Helmet to enhance your API's security
app.use(cors());

// enabling CORS for all requests
app.use(helmet());

// parse JSON bodies into JS objects
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// adding morgan to log HTTP requests
app.use(morgan('combined'));

// ----------------------------------------------------------------
// All Routes

app.use(router);
// ----------------------------------------------------------------
// Starting the Server

app.listen(config.port, async () => {
  console.log(`listening on http://localhost:${config.port}`);

  try {
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');

   Runner.connectionOptions({
      host: config.host,
      user: config.username,
      password: config.password,
      port: config.dbport,
      dialect: config.dialect,
      database: config.database,
   });

   const file1_path = `${__dirname}\\scripts\\walking_rabbit.sql`;

   Runner.runFile(file1_path, (err)=>{
     if(err){
         console.log(err);
     } else {
         console.log("Script sucessfully executed!");
     }
   });

  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
});
