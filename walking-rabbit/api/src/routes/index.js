const { Router } = require('express');
const router = Router();
const categoriesRouter = require('./categories.route');
const beveragesRouter = require('./beverages.route');
const billsRouter = require('./bills.route');
const machinesRoute = require('./machines.route');

router.get('/', (_, res) => res.send({ message: 'Welcome to the Application API' }));
router.get('/status', (_, res) => res.sendStatus(200));

router.use('/api/categories', categoriesRouter);
router.use('/api/beverage', beveragesRouter);
router.use('/api/bill', billsRouter);
router.use('/api/machine', machinesRoute);

module.exports = router;
