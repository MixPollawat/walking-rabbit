const express = require('express');

const {
  createBill,
  getBills,
  getAllMoney,
} = require('../controllers/beverage.controller');

const router = express.Router();

router.route('/').post(createBill).get(getBills);
router.route('/sumMoney').get(getAllMoney);

module.exports = router;
