const express = require('express');

const {
  createBaverage,
  getBaverages,
  getBaverage,
  getBaverageByCategoryId,
  updateBaverage,
  deleteBaverage,
  calBaverage,
} = require('../controllers/beverage.controller');

const router = express.Router();

router.route('/').post(createBaverage).get(getBaverages);
router.route('/calBaverage').post(calBaverage);
router.route('/getByCategory/:id').get(getBaverageByCategoryId);
router.route('/:id').get(getBaverage).patch(updateBaverage).delete(deleteBaverage);

module.exports = router;
