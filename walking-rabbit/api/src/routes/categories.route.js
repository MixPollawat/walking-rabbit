const express = require('express');
const {
  createCategory,
  getCattegories,
  getCategory,
  updateCategory,
  deleteCategory,
} = require('../controllers/beverage.controller');

const router = express.Router();

router.route('/').post(createCategory).get(getCattegories);
router.route('/:id').get(getCategory).patch(updateCategory).delete(deleteCategory);

module.exports = router;
