const express = require('express');

const {
  getMachines,
  updateMachines,
} = require('../controllers/beverage.controller');

const router = express.Router();

router.route('/').patch(updateMachines).get(getMachines);

module.exports = router;
